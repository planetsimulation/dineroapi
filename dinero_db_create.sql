
    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKcrc3dkkmtsxefbxgroei6yqk0 
       foreign key (user_id) 
       references category (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)

    create table answer (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        description varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table category (
       id bigint not null auto_increment,
        created_at datetime,
        details varchar(255) not null,
        name varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table choice (
       id bigint not null auto_increment,
        first_choice varchar(255) not null,
        fourth_choice varchar(255) not null,
        second_choice varchar(255) not null,
        third_choice varchar(255) not null,
        primary key (id)
    ) engine=InnoDB

    create table history (
       id bigint not null auto_increment,
        credits integer not null,
        played_at datetime,
        points integer not null,
        total_points integer not null,
        type varchar(255) not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table question (
       id bigint not null auto_increment,
        content varchar(255) not null,
        created_at datetime,
        xp integer not null,
        answer_id bigint not null,
        choice_id bigint not null,
        room_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table result (
       id bigint not null auto_increment,
        created_at datetime,
        time_stamp datetime not null,
        room_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table room (
       id bigint not null auto_increment,
        created_at datetime,
        price integer not null,
        type varchar(255) not null,
        category_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        created_at datetime,
        email varchar(255),
        first_name varchar(255) not null,
        last_name varchar(255) not null,
        password varchar(255),
        role integer not null,
        xp integer not null,
        primary key (id)
    ) engine=InnoDB

    alter table question 
       add constraint UK_s79pxfrw1xprt83grseyr98w6 unique (answer_id)

    alter table question 
       add constraint UK_rhdkktww0uvsd7md2vsbgrkqa unique (choice_id)

    alter table user 
       add constraint UKob8kqyqqgmefl0aco34akdtpe unique (email)

    alter table history 
       add constraint FKoxuy0cgrfcrk3qkaj3x10qsh9 
       foreign key (room_id) 
       references room (id)

    alter table history 
       add constraint FKn4gjyu69m6xa5f3bot571imbe 
       foreign key (user_id) 
       references user (id)

    alter table question 
       add constraint FK2w9qd6mx9oh2vchntaokhlj4f 
       foreign key (answer_id) 
       references answer (id)

    alter table question 
       add constraint FKotjsxxfrm7rpi7avxehl327m8 
       foreign key (choice_id) 
       references choice (id)

    alter table question 
       add constraint FK8uj5u1ki48awftnyabhfgpke 
       foreign key (room_id) 
       references room (id)

    alter table result 
       add constraint FK5rv7bbsn7hp9sbswu8e5jhsmw 
       foreign key (room_id) 
       references user (id)

    alter table result 
       add constraint FKpjjrrf0483ih2cvyfmx70a16b 
       foreign key (user_id) 
       references user (id)

    alter table room 
       add constraint FKllkgnps1iryk3347aokxwbxxm 
       foreign key (category_id) 
       references category (id)
