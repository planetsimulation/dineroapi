package com.planetsimulation.dinero.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.planetsimulation.dinero.domain.Room;
import com.planetsimulation.dinero.domain.Room.Status;
import com.planetsimulation.dinero.repositories.RoomRepository;

@Service
public class RoomServiceImpl implements RoomService {

	RoomRepository roomRepository;

	@Autowired
	public RoomServiceImpl(RoomRepository roomRepo) {
		this.roomRepository = roomRepo;
	}
	
	@Override
	public Room save(Room room) {
		return this.roomRepository.save(room);
	}

	@Override
	public Iterable<Room> saveAll(Iterable<Room> rooms) {
		return this.roomRepository.saveAll(rooms);
	}

	@Override
	public Optional<Room> findById(Long id) {
		return this.roomRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return this.roomRepository.existsById(id);
	}

	@Override
	public Iterable<Room> findAll() {
		return this.roomRepository.findAll();
	}

	@Override
	public Iterable<Room> findAllById(Iterable<Long> ids) {
		return this.roomRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return this.roomRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		this.roomRepository.deleteById(id);
	}

	@Override
	public void delete(Room room) {
		this.roomRepository.delete(room);
	}

	@Override
	public void deleteAll(Iterable<Room> rooms) {
		this.roomRepository.deleteAll(rooms);
	}

	@Override
	public void deleteAll() {
		this.roomRepository.deleteAll();
	}

	@Override
	public Iterable<Room> findAllByType(Status status) {
		return this.roomRepository.findRoomByType(status);
	}
		
}