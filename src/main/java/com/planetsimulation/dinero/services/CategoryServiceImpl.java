package com.planetsimulation.dinero.services;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.planetsimulation.dinero.domain.Category;
import com.planetsimulation.dinero.repositories.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService {

	CategoryRepository categoryRepository;
	
	public CategoryServiceImpl(CategoryRepository categoryRepository) {
		// TODO Auto-generated constructor stub
		this.categoryRepository=categoryRepository;
	}
	@Override
	public Category save(Category category) {
		return this.categoryRepository.save(category);
	}

	@Override
	public Iterable<Category> saveAll(Iterable<Category> catagories) {
		return this.categoryRepository.saveAll(catagories);
	}

	@Override
	public Optional<Category> findById(Long id) {
		return this.categoryRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return this.categoryRepository.existsById(id);
	}

	@Override
	public Iterable<Category> findAll() {
		return this.categoryRepository.findAll();
	}

	@Override
	public Iterable<Category> findAllById(Iterable<Long> ids) {
		return this.categoryRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return this.categoryRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		this.categoryRepository.deleteById(id);
		
	}

	@Override
	public void delete(Category category) {
		this.delete(category);
		
	}

	@Override
	public void deleteAll(Iterable<Category> categories) {
		this.deleteAll();
		
	}

	@Override
	public void deleteAll() {
		this.deleteAll();
		
	}
	
	

}
