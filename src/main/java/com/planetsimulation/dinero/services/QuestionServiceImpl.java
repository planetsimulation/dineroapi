package com.planetsimulation.dinero.services;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.planetsimulation.dinero.domain.Question;
import com.planetsimulation.dinero.repositories.QuestionRepository;

@Service
public class QuestionServiceImpl implements QuestionService {
	
	QuestionRepository questionRepository;
	
	public QuestionServiceImpl(QuestionRepository questionRepository) {
		// TODO Auto-generated constructor stub
		this.questionRepository=questionRepository;
	}
	
	
	@Override
	public Question save(Question question) {
		return this.questionRepository.save(question);
	}

	@Override
	public Iterable<Question> saveAll(Iterable<Question> questions) {
		return this.questionRepository.saveAll(questions);
	}

	@Override
	public Optional<Question> findById(Long id) {
		return this.questionRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return this.questionRepository.existsById(id);
	}

	@Override
	public Iterable<Question> findAll() {
		return this.questionRepository.findAll();
	}

	@Override
	public Iterable<Question> findAllById(Iterable<Long> ids) {
		return this.questionRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return this.questionRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		this.questionRepository.deleteById(id);
		
	}

	@Override
	public void delete(Question question) {
		this.questionRepository.delete(question);
		
	}

	@Override
	public void deleteAll(Iterable<Question> questions) {
		this.questionRepository.deleteAll(questions);
		
	}

	@Override
	public void deleteAll() {
		this.questionRepository.deleteAll();
		
	}

}
