package com.planetsimulation.dinero.services;

import java.util.Optional;

import com.planetsimulation.dinero.domain.Room;
import com.planetsimulation.dinero.domain.Room.Status;



public interface RoomService {
	
	public Room save(Room room);
	
	public Iterable<Room> saveAll(Iterable<Room> rooms);

	Optional<Room> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Room> findAll();

	Iterable<Room> findAllById(Iterable<Long> ids);
	
	Iterable<Room> findAllByType(Status status);

	long count();
	
	void deleteById(Long id);
	
	void delete(Room room);
	
	void deleteAll(Iterable<Room> rooms);

	void deleteAll();

}
