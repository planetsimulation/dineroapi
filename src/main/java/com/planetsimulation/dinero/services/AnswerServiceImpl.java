package com.planetsimulation.dinero.services;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.planetsimulation.dinero.domain.Answer;
import com.planetsimulation.dinero.repositories.AnswerRepository;

@Service
public class AnswerServiceImpl implements AnswerService {
	
	AnswerRepository answerRepository;
	
	public AnswerServiceImpl(AnswerRepository answerRepository) {
		this.answerRepository=answerRepository;
	}

	@Override
	public Answer save(Answer answer) {
		return answerRepository.save(answer);
	}

	@Override
	public Iterable<Answer> saveAll(Iterable<Answer> answers) {
		return answerRepository.saveAll(answers);
	}

	@Override
	public Optional<Answer> findById(Long id) {
		return answerRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Answer> findAll() {
		return answerRepository.findAll();
	}

	@Override
	public Iterable<Answer> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Answer answer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<Answer> answers) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

}
