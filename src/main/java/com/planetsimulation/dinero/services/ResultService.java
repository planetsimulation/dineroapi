package com.planetsimulation.dinero.services;

import java.util.Optional;

import com.planetsimulation.dinero.domain.Result;



public interface ResultService {
	
	public Result save (Result result);
	
	public Iterable<Result> saveAll(Iterable<Result> results);

	Optional<Result> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Result> findAll();

	Iterable<Result> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Result result);
	
	void deleteAll(Iterable<Result> result);

	void deleteAll();

}
