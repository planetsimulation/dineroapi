package com.planetsimulation.dinero.services;

import java.util.Optional;

import com.planetsimulation.dinero.domain.Choice;



public interface ChoiceService {

	public Choice save(Choice choice);
	
	public Iterable<Choice> saveAll(Iterable<Choice> choices);

	Optional<Choice> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Choice> findAll();

	Iterable<Choice> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Choice choice);
	
	void deleteAll(Iterable<Choice> choices);

	void deleteAll();
}
