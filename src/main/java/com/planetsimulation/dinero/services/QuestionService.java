package com.planetsimulation.dinero.services;

import java.util.Optional;

import com.planetsimulation.dinero.domain.Question;

public interface QuestionService {

	public Question save(Question question);
	
	public Iterable<Question> saveAll(Iterable<Question> quations);

	Optional<Question> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Question> findAll();

	Iterable<Question> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Question question);
	
	void deleteAll(Iterable<Question> questions);

	void deleteAll();
}
