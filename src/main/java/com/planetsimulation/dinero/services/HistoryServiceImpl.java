package com.planetsimulation.dinero.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.planetsimulation.dinero.domain.History;
import com.planetsimulation.dinero.domain.User;
import com.planetsimulation.dinero.repositories.HistoryRepository;
import com.planetsimulation.dinero.repositories.RoomRepository;

@Service
public class HistoryServiceImpl implements HistoryService {

	HistoryRepository historyRepository;
	
	@Autowired
	public HistoryServiceImpl(HistoryRepository historyRepository) {
		this.historyRepository = historyRepository;
	}
	
	@Override
	public History save(History history) {
		// TODO Auto-generated method stub
		return this.historyRepository.save(history);
	}

	@Override
	public Iterable<History> saveAll(Iterable<History> histories) {
		// TODO Auto-generated method stub
		return this.historyRepository.saveAll(histories);
	}

	@Override
	public Optional<History> findById(Long id) {
		// TODO Auto-generated method stub
		return this.historyRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return this.historyRepository.existsById(id);
	}

	@Override
	public Iterable<History> findAll() {
		// TODO Auto-generated method stub
		return this.historyRepository.findAll();
	}

	@Override
	public Iterable<History> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return this.historyRepository.findAllById(ids);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return this.historyRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		this.historyRepository.deleteById(id);
	}

	@Override
	public void delete(History history) {
		// TODO Auto-generated method stub
		this.historyRepository.delete(history);
	}

	@Override
	public void deleteAll(Iterable<History> histories) {
		// TODO Auto-generated method stub
		this.historyRepository.deleteAll(histories);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		this.historyRepository.deleteAll();
		
	}

	@Override
	public Iterable<History> findAllByUser(User user) {
		return this.historyRepository.findHistoryByUser(user);
	}

}
