package com.planetsimulation.dinero.services;

import java.util.Optional;

import com.planetsimulation.dinero.domain.Category;



public interface CategoryService {

	public Category save(Category category);
	
	public Iterable<Category> saveAll(Iterable<Category> catagories);

	Optional<Category> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Category> findAll();

	Iterable<Category> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Category category);
	
	void deleteAll(Iterable<Category> categories);

	void deleteAll();
}
