package com.planetsimulation.dinero.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.planetsimulation.dinero.domain.User;
import com.planetsimulation.dinero.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	UserRepository userRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository=userRepository;
	}

	@Override
	public User save(User user) {
		return this.userRepository.save(user);
	}

	@Override
	public Iterable<User> saveAll(Iterable<User> users) {
		return this.userRepository.saveAll(users);
	}

	@Override
	public Optional<User> findById(Long id) {
		return this.userRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return this.userRepository.existsById(id);
	}

	@Override
	public Iterable<User> findAll() {
		return this.userRepository.findAll();
	}

	@Override
	public Iterable<User> findAllById(Iterable<Long> ids) {
		return this.userRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return this.userRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		this.deleteById(id);
	}

	@Override
	public void delete(User user) {
		this.delete(user);
	}

	@Override
	public void deleteAll(Iterable<User> users) {
		this.deleteAll();
	}

	@Override
	public void deleteAll() {
		this.deleteAll();
	}

	@Override
	public Optional<User> findByEmail(String email) {
		return this.userRepository.findUserByEmail(email);
	}

}
