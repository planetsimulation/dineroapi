package com.planetsimulation.dinero.services;

import java.util.Optional;

import com.planetsimulation.dinero.domain.History;
import com.planetsimulation.dinero.domain.User;

public interface HistoryService {

	public History save(History history);
	
	public Iterable<History> saveAll(Iterable<History> histories);

	Optional<History> findById(Long id);

	public Iterable<History> findAllByUser(User user);
	
	boolean existsById(Long id);
	
	Iterable<History> findAll();

	Iterable<History> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(History History);
	
	void deleteAll(Iterable<History> Histories);

	void deleteAll();
	
}
