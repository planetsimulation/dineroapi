package com.planetsimulation.dinero.services;

import java.util.Optional;

import com.planetsimulation.dinero.domain.Answer;


public interface AnswerService {

	public Answer save(Answer answer);
	
	public Iterable<Answer> saveAll(Iterable<Answer> answers);

	Optional<Answer> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Answer> findAll();

	Iterable<Answer> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Answer answer);
	
	void deleteAll(Iterable<Answer> answers);

	void deleteAll();
}
