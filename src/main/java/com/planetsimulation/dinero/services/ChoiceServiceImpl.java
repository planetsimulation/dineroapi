package com.planetsimulation.dinero.services;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.planetsimulation.dinero.domain.Choice;
import com.planetsimulation.dinero.repositories.ChoiceRepository;

@Service
public class ChoiceServiceImpl implements ChoiceService {
	
	ChoiceRepository choiceRepository;
	
	public ChoiceServiceImpl(ChoiceRepository choiceRepository) {
		// TODO Auto-generated constructor stub
		this.choiceRepository=choiceRepository;
	}

	@Override
	public Choice save(Choice choice) {
		return choiceRepository.save(choice);
	}

	@Override
	public Iterable<Choice> saveAll(Iterable<Choice> choices) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Choice> findById(Long id) {
		return choiceRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Choice> findAll() {
		return choiceRepository.findAll();
	}

	@Override
	public Iterable<Choice> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Choice choice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<Choice> choices) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}
	

}
