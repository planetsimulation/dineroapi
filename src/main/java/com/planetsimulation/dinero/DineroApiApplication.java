package com.planetsimulation.dinero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DineroApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DineroApiApplication.class, args);
	}

}
