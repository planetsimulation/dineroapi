package com.planetsimulation.dinero.api;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.planetsimulation.dinero.domain.Answer;
import com.planetsimulation.dinero.domain.Choice;
import com.planetsimulation.dinero.services.AnswerService;
import com.planetsimulation.dinero.services.ChoiceService;

@RestController
@RequestMapping(path="/choice",produces="application/json")
@CrossOrigin(origins="*")
public class ChoiceController {
	
	private ChoiceService choiceService;
	
	public ChoiceController(ChoiceService choiceService) {
		this.choiceService=choiceService;
	}
	
	@GetMapping("/all")
	public Iterable<Choice> allChoices(){
		return choiceService.findAll();
	}
	
	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Choice postChoice(@RequestBody Choice choice) {
		return choiceService.save(choice);
	}
	
	@PutMapping(path="/{id}",consumes="application/json")
	public Choice updateChoice(@RequestBody Choice choice) {
		return choiceService.save(choice);
	}
	
	@DeleteMapping(path="{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT)
	public void deleteChoice(@PathVariable("id") long id) {
		try {
			choiceService.deleteById(id);
		}
		catch(EmptyResultDataAccessException e){
			
		}
	}

}
