package com.planetsimulation.dinero.api;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.planetsimulation.dinero.domain.Answer;
import com.planetsimulation.dinero.domain.Category;
import com.planetsimulation.dinero.services.AnswerService;
import com.planetsimulation.dinero.services.CategoryService;

@RestController
@RequestMapping(path="/answer",produces="application/json")
@CrossOrigin(origins="*")

public class AnswerController {
	
	private AnswerService answerService;
	
	public AnswerController(AnswerService answerService) {
		this.answerService=answerService;
	}
	
	@GetMapping("/all")
	public Iterable<Answer> allAnswers(){
		return answerService.findAll();
	}
	
	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Answer postAnswer(@RequestBody Answer answer) {
		return answerService.save(answer);
	}
	
	@PutMapping(path="/{id}",consumes="application/json")
	public Answer updateAnswer(@RequestBody Answer answer) {
		return answerService.save(answer);
	}
	
	@DeleteMapping(path="{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT)
	public void deleteAnswer(@PathVariable("id") long id) {
		try {
			answerService.deleteById(id);
		}
		catch(EmptyResultDataAccessException e){
			
		}
	}
	
	

}
