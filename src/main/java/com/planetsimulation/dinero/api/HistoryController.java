package com.planetsimulation.dinero.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.planetsimulation.dinero.domain.History;
import com.planetsimulation.dinero.domain.Room;
import com.planetsimulation.dinero.domain.User;
import com.planetsimulation.dinero.services.HistoryService;
import com.planetsimulation.dinero.services.HistoryServiceImpl;
import com.planetsimulation.dinero.services.RoomService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path="/history",produces="application/json")
@CrossOrigin(origins="*")
@Slf4j
public class HistoryController {
	public HistoryService historyService;
	@Autowired
	public HistoryController(HistoryService historyService) {
		// TODO Auto-generated constructor stub
		this.historyService = historyService;
	}

	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Iterable<History> allHistoriesById(@RequestBody User user) {
		return historyService.findAllByUser(user);
	}
	
	@PostMapping(path="/add", consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public History insertHistory(@RequestBody History history){
		log.info("-------------->","hey"+ history.getRoom().getId());
		return historyService.save(history);
//		return 1;
	}

}
