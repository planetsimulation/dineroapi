package com.planetsimulation.dinero.api;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.planetsimulation.dinero.domain.Answer;
import com.planetsimulation.dinero.domain.Question;
import com.planetsimulation.dinero.services.QuestionService;

@RestController
@RequestMapping(path="/question", produces="application/json")
@CrossOrigin(origins="*")
public class QuestionController {
	
	private QuestionService questionService;
	
	public QuestionController(QuestionService questionService) {
		this.questionService=questionService;
	}
	
	@GetMapping("/all")
	public Iterable<Question> allQuestions(){
		return questionService.findAll();
	}
	
	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Question addQuestion(@RequestBody Question question) {
		return this.questionService.save(question);
	}
	
	@PutMapping(path="/{id}",consumes="application/json")
	public Question updateQuestion(@RequestBody Question question) {
		return questionService.save(question);
	}
	
	@DeleteMapping(path="{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT)
	public void deleteQuestion(@PathVariable("id") long id) {
		try {
			questionService.deleteById(id);
		}
		catch(EmptyResultDataAccessException e){
			
		}
	}
	

}
