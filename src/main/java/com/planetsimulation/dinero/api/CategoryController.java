package com.planetsimulation.dinero.api;

import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.planetsimulation.dinero.domain.Answer;
import com.planetsimulation.dinero.domain.Category;
import com.planetsimulation.dinero.domain.User;
import com.planetsimulation.dinero.services.CategoryService;

@RestController
@RequestMapping(path="/category",produces="application/json")
@CrossOrigin(origins="*")
public class CategoryController {
	
	private CategoryService categoryService;
	
	public CategoryController(CategoryService categoryService) {
		this.categoryService=categoryService;
	}
	
	@GetMapping("/all")
	public Iterable<Category> allCategories(){
		return categoryService.findAll();
	}
	
	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Category postCategory(@RequestBody Category category) {
		return categoryService.save(category);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Category> categoryById(@PathVariable("id") Long id){
		Optional<Category> optCategory= categoryService.findById(id);
		if(optCategory.isPresent()) {
			return new ResponseEntity<>(optCategory.get(),HttpStatus.OK);
		}
		return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	}
	
	@PutMapping(path="/{id}",consumes="application/json")
	public Category updateCategory(@RequestBody Category category) {
		return categoryService.save(category);
	}
	
	@DeleteMapping(path="{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT)
	public void deleteCategory(@PathVariable("id") long id) {
		try {
			categoryService.deleteById(id);
		}
		catch(EmptyResultDataAccessException e){
			
		}
	}

}
