package com.planetsimulation.dinero.api;

import java.util.Date;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.planetsimulation.dinero.domain.Answer;
import com.planetsimulation.dinero.domain.Room;
import com.planetsimulation.dinero.domain.Room.Status;
import com.planetsimulation.dinero.services.RoomService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path="/room",produces="application/json")
@CrossOrigin(origins="*")
@Slf4j
public class RoomController {

private RoomService roomService;
	@Autowired
	public RoomController(RoomService roomService) {
		this.roomService = roomService;
	}
	
	@GetMapping("all")
	 public Iterable<Room> allRooms() {
		return roomService.findAll();
	 }
	
	@GetMapping("active")
	public Iterable<Room> activeRooms(){
		Iterable<Room> rooms = roomService.findAllByType(Status.ACTIVE);
		log.info("");
		return rooms;
	}
	
	@PostMapping("add")
	public Room addRoom(@RequestBody Room room) {
		return roomService.save(room);
	}
	
	@PutMapping(path="/{id}",consumes="application/json")
	public Room updateRoom(@RequestBody Room room) {
		return roomService.save(room);
	}
	
	@DeleteMapping(path="{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT)
	public void deleteRoom(@PathVariable("id") long id) {
		try {
			roomService.deleteById(id);
		}
		catch(EmptyResultDataAccessException e){
			
		}
	}
}
