package com.planetsimulation.dinero.api;

import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.planetsimulation.dinero.domain.Answer;
import com.planetsimulation.dinero.domain.User;
import com.planetsimulation.dinero.services.UserService;

import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path="/user",produces="application/json")
@CrossOrigin(origins="*")
public class UserController {
	
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(UserController.class);

	private UserService userService;
	public UserController(UserService userService) {
		this.userService=userService;
	}
	
	@GetMapping("/all")
	public Iterable<User> allUsers(){
		return userService.findAll();
	}

	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public User postUser(@RequestBody User user) {
		return userService.save(user);
	}

	@GetMapping("/one/{email}")
	public User getUser(@PathVariable("email") String email) {
		return userService.findByEmail(email).get();
	}
	
	@PutMapping(path="/{id}",consumes="application/json")
	public User updateUser(@RequestBody User user) {
		return userService.save(user);
	}
	
	@DeleteMapping(path="{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT)
	public void deleteTaco(@PathVariable("id") long id) {
		try {
			userService.deleteById(id);
		}
		catch(EmptyResultDataAccessException e){
			
		}
	}


}
