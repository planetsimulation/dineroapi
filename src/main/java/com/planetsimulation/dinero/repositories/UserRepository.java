package com.planetsimulation.dinero.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.planetsimulation.dinero.domain.User;

public interface UserRepository extends CrudRepository<User,Long> {
	
	Optional<User> findUserByEmail(String email);
}
