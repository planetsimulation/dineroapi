package com.planetsimulation.dinero.repositories;

import org.springframework.data.repository.CrudRepository;

import com.planetsimulation.dinero.domain.Choice;



public interface ChoiceRepository extends CrudRepository<Choice, Long> {

}
