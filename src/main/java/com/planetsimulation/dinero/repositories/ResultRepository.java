package com.planetsimulation.dinero.repositories;

import org.springframework.data.repository.CrudRepository;

import com.planetsimulation.dinero.domain.Result;



public interface ResultRepository extends CrudRepository<Result, Long> {

}
