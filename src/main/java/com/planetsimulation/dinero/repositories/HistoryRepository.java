package com.planetsimulation.dinero.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.planetsimulation.dinero.domain.History;
import com.planetsimulation.dinero.domain.User;

public interface HistoryRepository extends CrudRepository<History, Long> {
	Iterable<History> findHistoryByUser(User user);
}
