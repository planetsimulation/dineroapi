package com.planetsimulation.dinero.repositories;

import org.springframework.data.repository.CrudRepository;

import com.planetsimulation.dinero.domain.Room;
import com.planetsimulation.dinero.domain.Room.Status;



public interface RoomRepository extends CrudRepository<Room, Long> {
	Iterable<Room> findRoomByType(Status status);
}
