package com.planetsimulation.dinero.repositories;

import org.springframework.data.repository.CrudRepository;

import com.planetsimulation.dinero.domain.Answer;



public interface AnswerRepository extends CrudRepository<Answer, Long> {

}
