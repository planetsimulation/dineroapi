package com.planetsimulation.dinero.repositories;

import org.springframework.data.repository.CrudRepository;

import com.planetsimulation.dinero.domain.Category;



public interface CategoryRepository extends CrudRepository<Category, Long> {

}
