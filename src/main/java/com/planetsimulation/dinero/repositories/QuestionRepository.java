package com.planetsimulation.dinero.repositories;

import org.springframework.data.repository.CrudRepository;

import com.planetsimulation.dinero.domain.Question;



public interface QuestionRepository extends CrudRepository<Question, Long> {

}
