package com.planetsimulation.dinero.domain;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;



import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Data
@Entity
public class Room {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Date createdAt;
	
	@PrePersist
	void createdAt() {
		this.createdAt = new Date();
	}
	
	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	private Status type = Status.INACTIVE;
	
	public static enum Status{
		ACTIVE,INACTIVE,COMPLETE
	}
	
	private int price;
	
	private String roomName;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER,targetEntity=Category.class)
	private Category category ;
	

}
