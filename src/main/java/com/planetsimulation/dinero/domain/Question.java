package com.planetsimulation.dinero.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class Question {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Date createdAt;
	
	@PrePersist
	void createdAt() {
		this.createdAt = new Date();
	}
	
	@NotNull
	public String content;

	@NotNull
	private int xp;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER, optional=false)
	private Answer answer;
	
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER,targetEntity=Room.class)
	private Room room;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER, optional=false)
	private Choice choice;

}
