package com.planetsimulation.dinero.domain;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import com.planetsimulation.dinero.domain.Room.Status;

import lombok.Data;

@Data
@Entity
public class History {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Date playedAt;
	
	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	private GameStatus type;
	
	private int credits;
	
	private int points;
	
	private int totalPoints;
	
	
	@PrePersist
	void playedAt() {
		this.playedAt = new Date();
	}
	
	public static enum GameStatus{
		WON, LOST
	}
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER,targetEntity=User.class)
	private User user;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER,targetEntity=Room.class)
	private Room room;
}
