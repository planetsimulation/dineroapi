package com.planetsimulation.dinero.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class Choice {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long id;
	
	@NotNull
	private String firstChoice;
	
	@NotNull
	private String secondChoice;
	
	@NotNull
	private String thirdChoice;
	
	@NotNull
	private String fourthChoice;

}
